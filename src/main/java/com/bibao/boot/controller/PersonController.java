package com.bibao.boot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.bibao.boot.model.Person;
import com.bibao.boot.service.PersonService;

@Controller
public class PersonController {
	@Autowired
	private PersonService service;
	
	@Value("welcome")
	private String welcomePage;
	
	@Value("home")
	private String homePage;
	
	@RequestMapping("/home")
	public String home() {
		return homePage;
	}
	
	@RequestMapping(value="/save", method=RequestMethod.POST)
	public ModelAndView execute(@ModelAttribute("person") Person person, BindingResult result) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName(welcomePage);
		mav.addObject("personInfo", service.process(person));
		return mav;
	}
}
