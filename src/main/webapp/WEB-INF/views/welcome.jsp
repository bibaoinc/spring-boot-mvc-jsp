<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome Page</title>
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
</head>
<body>
<div class="container">
	<h2>Welcome Page</h2>
	<%@page import="com.bibao.boot.model.Person" %>
	<jsp:useBean id="personInfo" scope="request" class="com.bibao.boot.model.PersonInfo"/>
	<h2><jsp:getProperty property="message" name="personInfo"/></h2>
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>First Name</th>
				<th>Last Name</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="person" items="${personInfo.personList}">
				<tr>
					<td>${person.id}</td>
					<td>${person.firstName}</td>
					<td>${person.lastName}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
</body>
</html>