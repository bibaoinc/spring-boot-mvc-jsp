<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home Page</title>
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
</head>
<body>
<div class="container">
	<h1>Spring Boot MVC Demo</h1>
	<hr>
	<h2>Home Page</h2>
	<div class="col-sm-6">
		<form class="form" role="form" action="/save" method="POST">
			<div class="form-group">
				<label for="firstName">First Name</label>
				<input type="text" id="firstName" name="firstName" class="form-control"/>
			</div>
			<div class="form-group">
				<label for="lastName">Last Name</label>
				<input type="text" id="lastName" name="lastName" class="form-control"/>
			</div>
			<div>
				<button type="reset" class="btn btn-primary">Clear</button>
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
		</form>
	</div>
</div>
</body>
</html>